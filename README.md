Krona ORM Module
==============
[![Scrutinizer Quality Score](https://scrutinizer-ci.com/b/Arilas/orm-module/badges/quality-score.png?s=d139aa2bd03fb7535bff19e68eff1e20b9938a0a)](https://scrutinizer-ci.com/b/Arilas/orm-module/)

[![wercker status](https://app.wercker.com/status/40f78f86a506649bc8a132f4ca6c4605/m "wercker status")](https://app.wercker.com/project/bykey/40f78f86a506649bc8a132f4ca6c4605)

[![Code Coverage](https://scrutinizer-ci.com/b/Arilas/orm-module/badges/coverage.png?s=1781b092a986776e69819b434c823a9791a59f68)](https://scrutinizer-ci.com/b/Arilas/orm-module/)
## Install

### Composer

Install Composer [Composer](http://getcomposer.org):

```bash
$ curl -s https://getcomposer.org/installer | php
```

Add requirements to `composer.json`:

```yaml
{
    "require": {
        "arilas/orm-module": "dev-master"
    }
}
```
