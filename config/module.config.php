<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/20/13
 * Time: 4:16 PM
 */
return [
    'controller_plugins' => [
        'invokables' => [
            'getArilas' => \Arilas\ORM\Mvc\Controller\Plugin\GetArilas::class,
            'createInputFilter' => \Arilas\ORM\Mvc\Controller\Plugin\CreateInputFilter::class,
        ],
    ],
    'arilas' => [
        'orm' => [
            'configuration' => [
                'orm_default' => [
                    'debug' => true,
                    'cache_dir' => 'data/ArilasORM/Cache',
                ],
            ],
            'types' => [
                'converters' => [
                    'date' => \Arilas\ORM\Common\Converter\DatetimeConverter::class,
                    'datetime' => \Arilas\ORM\Common\Converter\DatetimeConverter::class,
                    'association' => \Arilas\ORM\Common\Converter\AssociationConverter::class,
                    'array' => \Arilas\ORM\Common\Converter\ArrayConverter::class,
                    'integer' => \Arilas\ORM\Common\Converter\IntegerConverter::class,
                ],
            ],
        ],
        'form' => [
            'em' => '',
        ],
    ],
];
