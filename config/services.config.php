<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/20/13
 * Time: 4:16 PM
 */
return [
    'factories' => [
        'arilas.orm.entity_manager' => new Arilas\ORM\Service\Factory(),
        'arilas.orm.authentication_service' => new Arilas\ORM\Authentication\Service(),
    ],
    'invokables' => [
        'EntityStrategy' => \Arilas\ORM\View\Strategy\EntityStrategy::class,
    ]
];
