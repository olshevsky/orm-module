<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 10/4/13
 * Time: 1:53 PM
 */

namespace Arilas\ORM\Authentication;

use Arilas\ORM\Authentication\Storage\Session;
use Arilas\ORM\Repository\AbstractRepository;
use DoctrineModule\Authentication\Adapter\ObjectRepository;
use DoctrineModule\Options\Authentication as AuthenticationOptions;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\AbstractContainer;

class Service implements FactoryInterface
{
    protected $instance;

    /**
     * Create service
     *
     * @param  ServiceLocatorInterface $serviceLocator
     * @return AuthenticationService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        if (is_null($this->instance)) {
            $this->setAuthenticationService($serviceLocator);
        }

        return $this->instance;
    }

    protected function setAuthenticationService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        $sessionManager = AbstractContainer::getDefaultManager();

        $config = $config['doctrine']['authentication']['orm_default'];
        /** @var AbstractRepository $repository */
        $repository = $serviceLocator->get('arilas.orm.entity_manager')->getRepository($config['identity_class']);

        $options = new AuthenticationOptions();
        $options->setObjectRepository($repository);
        $options->setIdentityProperty($config['identity_property']);
        $options->setCredentialProperty($config['credential_property']);
        $options->setClassMetadata($repository->getClassMetadata());
        $options->setIdentityClass($config['identity_class']);
        $options->setCredentialCallable($config['credential_callable']);

        $adapter = new ObjectRepository($options);
        $authService = new AuthenticationService(
            new Session(null, null, $sessionManager),
            $adapter
        );
        $this->instance = $authService;
    }
}
