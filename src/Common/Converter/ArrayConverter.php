<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 6:43
 */

namespace Arilas\ORM\Common\Converter;


class ArrayConverter implements ConverterInterface
{

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        return json_decode($value, true);
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        return json_encode($value);
    }
}