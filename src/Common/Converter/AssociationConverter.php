<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/5/14
 * Time: 12:44 PM
 */

namespace Arilas\ORM\Common\Converter;


use Arilas\ORM\Entity\EntityInterface;

class AssociationConverter implements ConverterInterface
{

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        return $value;
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        if ($value == '') {
            return null;
        } elseif ($value instanceof EntityInterface) {
            return (int)$value->getId();
        } else {
            return (int)$value;
        }
    }
}