<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 1:34
 */

namespace Arilas\ORM\Common\Converter;


interface ConverterInterface
{
    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value);

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value);
} 