<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 1:34
 */

namespace Arilas\ORM\Common\Converter;


use DateTime;

class DatetimeConverter implements ConverterInterface
{
    const SQL_DATETIME = 'Y-m-d H:i:s';

    /**
     * Convert to PHP type
     * @param $value
     * @return DateTime
     */
    public function convert($value)
    {
        if ($value != '') {
            return new DateTime($value);
        } else {
            return null;
        }
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        if ($value instanceof DateTime) {
            return $value->format(static::SQL_DATETIME);
        } else {
            return $value;
        }
    }
}