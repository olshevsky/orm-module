<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 6:53
 */

namespace Arilas\ORM\Common\Converter;


use Arilas\ORM\Entity\EntityInterface;

class DefaultConverter implements ConverterInterface
{

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        return $value;
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        if ($value instanceof EntityInterface) {
            return $value->getId();
        }
        return $value;
    }
}