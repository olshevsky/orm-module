<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 2:02
 */

namespace Arilas\ORM\Common;


use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\EntityManager;
use Arilas\ORM\Repository\AbstractRepository;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Class UnitOfWork
 * @package Arilas\ORM\Common
 */
class UnitOfWork
{
    /** @var EntityInterface[] */
    protected $instances = [];
    /** @var array[] */
    protected $origins = [];
    /** @var ClassMetadata */
    protected $classMetadata;
    /** @var EntityManager */
    protected $em;
    /** @var  string */
    protected $className;

    public function __construct(AbstractRepository $repository)
    {
        $this->classMetadata = $repository->getClassMetadata();
        $this->className = $this->classMetadata->getName();
        $this->em = $repository->getEntityManager();
    }

    /**
     * remove Entity from Unit Of Work
     * @param $id
     */
    public function remove($id)
    {
        unset($this->instances[$id]);
        unset($this->origins[$id]);
    }

    /**
     * @param array $data
     * @return EntityInterface
     */
    public function hydrate(array $data)
    {
        /** @var EntityInterface $object */
        $object = $this->em->getObjectHydrator()->hydrate(
            $data,
            new $this->className
        );

        if ($this->has($object->getId())) {
            $this->merge($object->getId(), $data);
        } else {
            $this->push($object);
        }

        return $object;
    }

    /**
     * @param $id
     * @return bool
     */
    public function has($id)
    {
        return isset($this->instances[$id]);
    }

    /**
     * Method that used to merge data in UnitOfWork and DB
     * @param $id
     * @param array $data
     * @return EntityInterface|bool
     */
    protected function merge($id, array $data)
    {
        $entity = $this->instances[$id];
        $this->em->getObjectHydrator()->hydrate($data, $entity);
        $this->register($id, $data);

        return $entity;
    }

    /**
     * Method for registering origins of Fetched data from DB
     * @param $id
     * @param $data
     */
    protected function register($id, $data)
    {
        $this->origins[$id] = $data;
    }

    /**
     * @param EntityInterface $entity
     */
    public function push(EntityInterface $entity)
    {
        $this->instances[$entity->getId()] = $entity;

        if (!isset($this->origins[$entity->getId()])) {
            $this->register(
                $entity->getId(),
                $this->em->getObjectHydrator()->extract($entity)
            );
        }
    }

    /**
     * @param $id
     * @return EntityInterface|bool
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->instances[$id];
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return array|null
     */
    public function getOrigin($id)
    {
        if (!$this->has($id)) {
            return null;
        }
        return $this->origins[$id];
    }

    public function updateOrigin(EntityInterface $entity)
    {
        $this->origins[$entity->getId()] = $this->em->getObjectHydrator()->extract($entity);
    }

    public function computeChangeSets()
    {
        $entitiesToProcess = [];
        foreach ($this->instances as $id => $entity) {
            $changes = $this->calculateChanges($entity);
            if (!empty($changes)) {
                $entitiesToProcess[$id] = [
                    $entity,
                    $changes,
                ];
            }
        }

        return $entitiesToProcess;
    }

    /**
     * @param EntityInterface $entity
     * @return array
     */
    public function calculateChanges(EntityInterface $entity)
    {
        if (!is_null($entity->getId()) && isset($this->origins[$entity->getId()])) {
            $origin = $this->origins[$entity->getId()];
            return $this->arrayDiff(
                $this->em->getObjectHydrator()->extract($entity),
                $origin
            );
        } else {
            return $this->em->getObjectHydrator()->extract($entity);
        }
    }

    /**
     * Fix for working with int => string diff
     * @param $current
     * @param $origin
     * @return array
     */
    protected function arrayDiff($current, $origin)
    {
        $diff = [];

        foreach($current as $key => $value) {
            if($value . '' !== $origin[$key] . '') {
                $diff[$key] = $value;
            }
        }

        return $diff;
    }

    public function clear()
    {
        $this->origins = [];
        $this->instances = [];
    }
}