<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 8/21/14
 * Time: 4:39 PM
 */

namespace Arilas\ORM\DBAL;


use Arilas\ORM\EntityManager;
use Doctrine\DBAL\Query\QueryBuilder as BaseQueryBuilder;

class QueryBuilder extends BaseQueryBuilder
{
    protected $entityManager;

    protected $className;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct($entityManager->getConnection());
    }

    public function from($from, $alias)
    {
        if (strpos($from, '\\') !== false) {
            $this->className = $from;
            $from = $this->entityManager->getClassMetadata($from)->getTableName();
        }

        return parent::from($from, $alias);
    }

    /**
     * @param $field
     * @param $className
     * @return string
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function column($field, $className = null)
    {
        if (is_null($className)) {
            $className = $this->className;
        }

        $metadata = $this->entityManager->getClassMetadata($className);

        if ($metadata->hasAssociation($field)) {
            return $metadata->getSingleAssociationJoinColumnName($field);
        } else {
            return $metadata->getColumnName($field);
        }
    }
} 