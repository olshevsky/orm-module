<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 2:05
 */

namespace Arilas\ORM\Entity;


/**
 * An Interface that Entity must implement
 * Interface EntityInterface
 * @package Arilas\ORM\Entity
 */
interface EntityInterface
{
    /**
     * Method that returns an Identifier of Entity
     * @return mixed
     */
    public function getId();

    /**
     * Method that used for setting Identifier of Entity
     * @param $id
     * @return void|$this
     */
    public function setId($id);
} 