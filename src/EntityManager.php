<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 10/4/13
 * Time: 3:25 PM
 */
namespace Arilas\ORM;

use Arilas\ORM\Common\ObjectHydrator;
use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\Exception\RuntimeException;
use Arilas\ORM\Repository\AbstractRepository;
use Arilas\ORM\Service\ConnectionFactory;
use Arilas\ORM\Service\MetadataFactory;
use Arilas\ORM\Service\RepositoryFactory;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Mapping\ClassMetadata;
use Zend\EventManager\EventManager;

/**
 * Class EntityManager
 *
 *  Entity Manager provides simple and useful Interface for Managing Entities and working with DB in one place.
 * @package ORM
 */
class EntityManager implements ObjectManager
{
    const PRE_DELETE_EVENT = 'orm.delete.pre';
    const PRE_UPDATE_EVENT = 'orm.update.pre';
    const PRE_INSERT_EVENT = 'orm.insert.pre';
    const POST_DELETE_EVENT = 'orm.delete.post';
    const POST_UPDATE_EVENT = 'orm.update.post';
    const POST_INSERT_EVENT = 'orm.insert.post';

    /** @var  array */
    protected $config;
    /** @var MetadataFactory */
    protected $metadataFactory;
    /** @var  RepositoryFactory */
    protected $repositoryFactory;
    /** @var ConnectionFactory */
    protected $connectionFactory;
    /** @var  Connection */
    protected $connection;
    /** @var  EventManager */
    protected $events;
    /** @var  ObjectHydrator */
    protected $objectHydrator;

    public function __construct(EventManager $eventManager, ConnectionFactory $connectionFactory, array $config)
    {
        $this->setEventManager($eventManager);
        $this->connectionFactory = $connectionFactory;
        $this->config = $config;
        $this->metadataFactory = new MetadataFactory($this->config);
        $this->repositoryFactory = new RepositoryFactory($this);
    }

    public function setEventManager(EventManager $events)
    {
        $this->events = $events;
    }

    /**
     * Method uses for Commit Entity to DB
     * Example with insert:
     * <code>
     *  $user = new User();
     *  ...
     *  $em->commit($user);
     * </code>
     * Example with update:
     * <code>
     *  $user = $em->find(User::class, 1);
     *  ...
     *  $em->commit($user);
     * </code>
     * @param  EntityInterface $entity
     * @return int|string
     */
    public function commit(EntityInterface $entity)
    {
        return $this->getRepository(get_class($entity))->commit($entity);
    }

    /**
     * Method create instance of Repository by providing Entity Name
     * <code>
     * /** @var AbstractRepository $usersRepository
     * $usersRepository = $em->getRepository('User\Entity\User');
     * </code>
     * @param  string $entityName
     * @return AbstractRepository|null
     * @throws RuntimeException
     */
    public function getRepository($entityName)
    {
        return $this->repositoryFactory->getRepository($entityName);
    }

    /**
     * Method return an DB Event Manager for attach Pre/Post Update/Insert Listeners
     * @return EventManager
     */
    public function getEventManager()
    {
        return $this->events;
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        if (is_null($this->connection)) {
            $this->connection = 'orm_default';
        }
        if (is_string($this->connection)) {
            $this->setConnection($this->connection);
        }

        return $this->connection;
    }

    /**
     * @param  string|Connection|null $connection
     * @throws Exception\RuntimeException
     */
    public function setConnection($connection = null)
    {
        if ($connection instanceof Connection) {
            $this->connection = $connection;
        } elseif (is_string($connection)) {
            $this->connection = $this->connectionFactory->getConnection($connection);
        } else {
            throw new RuntimeException('Connection should be an instance of Connection class or string value');
        }
    }

    /**
     * Method uses for Remove Entity from DB
     * <code>
     *  $user = $em->find(User::class, 1);
     *  if ($user) {
     *      $em->remove($user);
     *  }
     * </code>
     * @param  EntityInterface $entity
     * @return bool|int
     */
    public function remove($entity)
    {
        return $this->getRepository(get_class($entity))->remove($entity);
    }

    /**
     * Method uses for Fetching Some Entity that have Relation with this table
     * <code>
     * $em
     *     ->setRelatedEntity($entity, [
     *         'primaryPhone',
     *         'creator' => [
     *             'contact',
     *         ],
     *     ]
     * ;
     * </code>
     * @param  EntityInterface $entity
     * @param  array $fields
     * @return EntityInterface
     */
    public function setRelatedEntity(EntityInterface $entity, array $fields)
    {
        foreach ($fields as $key => $field) {
            if (!is_int($key) && is_array($field)) {
                $this->setRelatedOne($entity, $key);
                if (!is_null($entity->{'get' . ucfirst($key)}())) {
                    $this->setRelatedEntity($entity->{'get' . ucfirst($key)}(), $field);
                }
            } elseif (is_string($field)) {
                $this->setRelatedOne($entity, $field);
            }
        }
        return $entity;
    }

    /**
     * @param EntityInterface $entity
     * @param $field
     * @return null
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    protected function setRelatedOne(EntityInterface $entity, $field)
    {
        if (
            method_exists($entity, 'get' . ucfirst($field))
            && $entity->{'get' . ucfirst($field)}() instanceof EntityInterface
        ) {
            return $entity->{'get' . ucfirst($field)}();
        }
        $metadata = $this->getClassMetadata(get_class($entity));
        $mapping = $metadata->getAssociationMapping($field);
        switch ($mapping['type']) {
            case ClassMetadata::MANY_TO_ONE:
            case ClassMetadata::ONE_TO_ONE:
                $identifier = $entity->{'get' . ucfirst($field)}();
                if (is_null($identifier)) {
                    return null;
                }
                $related = $this->find(
                    $mapping['targetEntity'],
                    $identifier
                );
                $entity->{'set' . ucfirst($field)}($related);

                break;
            case ClassMetadata::ONE_TO_MANY:
                $targetMetadata = $this->getClassMetadata($mapping['targetEntity']);
                $association = array_slice($targetMetadata->getAssociationsByTargetClass($metadata->getName()), 0);
                $targetMapping = array_pop($association);
                $targetColumnName = $targetMapping['joinColumns'][0]['name'];
                $identifier = [];
                foreach ($metadata->getIdentifier() as $id) {
                    $columnName = $metadata->getColumnName($id);
                    $identifier[$columnName] = $entity->getId();
                    break;
                }
                $related = $this->getRepository(
                    $mapping['targetEntity']
                )->findBy(
                    array(
                        $targetColumnName => $identifier[$targetMapping['joinColumns'][0]['referencedColumnName']],
                    )
                );
                method_exists($entity, 'set' . ucfirst($field)) && $entity->{'set' . ucfirst($field)}($related);

                break;
        }

        return null;
    }

    /**
     * Returns the ClassMetadata descriptor for a class.
     *
     * The class name must be the fully-qualified class name without a leading backslash
     * (as it is returned by get_class($obj)).
     *
     * @param string $className
     *
     * @return ClassMetadata
     */
    public function getClassMetadata($className)
    {
        return $this->getMetadataFactory()->getMetadataFor($className);
    }

    /**
     * Gets the metadata factory used to gather the metadata of classes.
     *
     * @return MetadataFactory
     */
    public function getMetadataFactory()
    {
        return $this->metadataFactory;
    }

    /**
     * Finds a object by its identifier.
     *
     * This is just a convenient shortcut for getRepository($className)->find($id).
     *
     * @param string $className The class name of the object to find.
     * @param mixed $id The identity of the object to find.
     *
     * @return EntityInterface|null The found object.
     */
    public function find($className, $id)
    {
        return $this->getRepository($className)->find($id);
    }

    /**
     * Fetch ObjectHydrator Instance
     * @return ObjectHydrator
     */
    public function getObjectHydrator()
    {
        if (is_null($this->objectHydrator)) {
            $this->objectHydrator = new ObjectHydrator($this, $this->config);
        }

        return $this->objectHydrator;
    }

    /**
     * Tells the ObjectManager to make an instance managed and persistent.
     *
     * The object will be entered into the database as a result of the flush operation.
     *
     * NOTE: The persist operation always considers objects that are not yet known to
     * this ObjectManager as NEW. Do not pass detached objects to the persist operation.
     *
     * @param object $object The instance to make managed and persistent.
     *
     * @return void
     */
    public function persist($object)
    {

    }

    /**
     * Merges the state of a detached object into the persistence context
     * of this ObjectManager and returns the managed copy of the object.
     * The object passed to merge will not become associated/managed with this ObjectManager.
     *
     * @param object $object
     *
     * @return object
     */
    public function merge($object)
    {
        return $object;
    }

    /**
     * Clears the ObjectManager. All objects that are currently managed
     * by this ObjectManager become detached.
     *
     * @param string|null $objectName if given, only objects of this type will get detached.
     *
     * @return void
     */
    public function clear($objectName = null)
    {
        if (is_null($objectName)) {
            $this->repositoryFactory->clearObjects();
        } else {
            $this->repositoryFactory->getRepository($objectName)->getUnitOfWork()->clear();
        }
    }

    /**
     * Detaches an object from the ObjectManager, causing a managed object to
     * become detached. Unflushed changes made to the object if any
     * (including removal of the object), will not be synchronized to the database.
     * Objects which previously referenced the detached object will continue to
     * reference it.
     *
     * @param object $object The object to detach.
     *
     * @return void
     */
    public function detach($object)
    {
        // TODO: Implement detach() method.
    }

    /**
     * Refreshes the persistent state of an object from the database,
     * overriding any local changes that have not yet been persisted.
     *
     * @param object $object The object to refresh.
     *
     * @return void
     */
    public function refresh($object)
    {
        // TODO: Implement refresh() method.
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * @return void
     */
    public function flush()
    {
        // TODO: Implement flush() method.
    }

    /**
     * Helper method to initialize a lazy loading proxy or persistent collection.
     *
     * This method is a no-op for other objects.
     *
     * @param object $obj
     *
     * @return void
     */
    public function initializeObject($obj)
    {
        // TODO: Implement initializeObject() method.
    }

    /**
     * Checks if the object is part of the current UnitOfWork and therefore managed.
     *
     * @param object $object
     *
     * @return bool
     */
    public function contains($object)
    {
        if ($object instanceof EntityInterface) {
            return $this->getRepository(get_class($object))->getUnitOfWork()->has($object->getId());
        } else {
            return false;
        }
    }
}
