<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/11/14
 * Time: 11:07 AM
 */

namespace Arilas\ORM\Form\Filter\Mapping;


use Arilas\ORM\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;

class Digits implements FilterMappingInterface
{

    public function attach(FilterChain $filterChain)
    {
        $filterChain->attach(new \Zend\Filter\Digits());
    }
}