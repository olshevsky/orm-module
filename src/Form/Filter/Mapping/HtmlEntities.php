<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:12
 */

namespace Arilas\ORM\Form\Filter\Mapping;

use Arilas\ORM\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;
use Zend\Filter\HtmlEntities as BaseHtmlEntities;

/**
 * Class HtmlEntities
 * @package Arilas\ORM\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class HtmlEntities implements FilterMappingInterface
{

    public function attach(FilterChain $filterChain)
    {
        $filterChain->attach(new BaseHtmlEntities());
    }
}