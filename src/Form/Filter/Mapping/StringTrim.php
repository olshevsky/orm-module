<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 0:55
 */

namespace Arilas\ORM\Form\Filter\Mapping;

use Arilas\ORM\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;
use Zend\Filter\StringTrim as BaseStringTrim;

/**
 * Class StringTrim
 * @package Arilas\ORM\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class StringTrim implements FilterMappingInterface
{
    public function attach(FilterChain $filterChain)
    {
        $filterChain->attach(new BaseStringTrim());
    }
}