<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 10:00
 */

namespace Arilas\ORM\Form\Mapping;

/**
 * Class InputFilter
 * @package Arilas\ORM\Form\Mapping
 * @Annotation
 * @Target({"CLASS"})
 */
class InputFilter
{
    public $serviceName;
} 