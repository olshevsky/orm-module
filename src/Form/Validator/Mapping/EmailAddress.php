<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 19.08.14
 * Time: 21:58
 */

namespace Arilas\ORM\Form\Validator\Mapping;


use Arilas\ORM\Form\Validator\ValidatorMappingInterface;
use Zend\InputFilter\Input;

/**
 * Class EmailAddress
 * @package Arilas\ORM\Form\Validator\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class EmailAddress implements ValidatorMappingInterface
{
    /** @var  string */
    public $message;

    public function attach(Input $input)
    {
        $input->getValidatorChain()
            ->attach(new \Zend\Validator\EmailAddress(
                (!is_null($this->message)?['message' => $this->message]:[])
            ));
    }
}