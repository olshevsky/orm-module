<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 4:56
 */

namespace Arilas\ORM\Form\Validator\Mapping;


use Arilas\ORM\Form\Validator\ValidatorMappingInterface;
use Zend\InputFilter\Input;

/**
 * Class Required
 * @package Arilas\ORM\Form\Validator\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Required implements ValidatorMappingInterface
{
    public function attach(Input $input)
    {
        $input->setRequired(true);
    }
}