<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 19.08.14
 * Time: 22:04
 */

namespace Arilas\ORM\Form\Validator\Mapping;

use Arilas\ORM\Form\Validator\UniqueEntity;
use Arilas\ORM\Form\Validator\ValidatorClassMappingInterface;
use Doctrine\Common\Persistence\ObjectManager as EntityManager;
use Zend\InputFilter\BaseInputFilter;

/**
 * Class UniqueFields
 * @package Arilas\ORM\Form\Validator\Mapping
 * @Annotation
 * @Target({"CLASS"})
 */
class UniqueFields implements ValidatorClassMappingInterface
{
    protected $fields = [];

    public function __construct($fields)
    {
        $fields = $fields['value'];
        if (!is_array($fields)) {
            $fields = [$fields];
        }
        $this->fields = $fields;
    }

    /**
     * @param BaseInputFilter $inputFilter
     * @param EntityManager $entityManager
     * @param $className
     */
    public function attach(BaseInputFilter $inputFilter, EntityManager $entityManager, $className)
    {
        foreach ($this->fields as $field) {
            $inputFilter->get($field)
                ->getValidatorChain()
                ->attach(
                    new UniqueEntity(
                        [
                            'entityManager' => $entityManager,
                            'checkField' => $field,
                            'entityClass' => $className,
                        ]
                    )
                );
        }
    }
}