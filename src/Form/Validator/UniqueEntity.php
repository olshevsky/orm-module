<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 27.08.14
 * Time: 0:06
 */

namespace Arilas\ORM\Form\Validator;

use Doctrine\Common\Persistence\ObjectManager as EntityManager;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class UniqueEntity extends AbstractValidator
{
    const ENTITY_FOUND = 'entityFound';
    protected $messageTemplates = array(
        self::ENTITY_FOUND => 'There is entity with this value'
    );

    /** @var  EntityManager */
    protected $entityManager;
    /** @var  string */
    protected $entityClass;

    protected $checkField;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @param null $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);
        if (null === $this->getEntityManager()) {
            throw new Exception\RuntimeException(__METHOD__ . ' There is no entityManager set.');
        }

        if (null === $this->getEntityClass()) {
            throw new Exception\RuntimeException(__METHOD__ . ' There is no entity class name set.');
        }
        $metadata = $this->getEntityManager()->getClassMetadata($this->getEntityClass());
        $identifier = $metadata->getIdentifierFieldNames();
        $identifier = array_shift($identifier);
        if (null === $this->getCheckField()) {
            $this->setCheckField($identifier);
        }

        if (is_array($context) && isset($context[$identifier]) && !is_null($context[$identifier])) {
            $entity = $this
                ->getEntityManager()
                ->getRepository($this->getEntityClass())->findOneBy([$this->getCheckField() => $this->getValue()]);

            if ($entity && $entity->getId() != $context[$identifier]) {
                $this->error(self::ENTITY_FOUND);

                return false;
            } else {
                return true;
            }
        } else {
            $entity = $this
                ->getEntityManager()
                ->getRepository($this->getEntityClass())->findOneBy([$this->getCheckField() => $this->getValue()]);

            // Set Error message
            if ($entity) {
                $this->error(self::ENTITY_FOUND);

                return false;
            } else {
                return true;
            }
        }
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * @param string $entityClass
     * @return $this
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheckField()
    {
        return $this->checkField;
    }

    /**
     * @param mixed $checkField
     * @return $this
     */
    public function setCheckField($checkField)
    {
        $this->checkField = $checkField;

        return $this;
    }
}