<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 19.08.14
 * Time: 22:16
 */

namespace Arilas\ORM\Form\Validator;

use Doctrine\Common\Persistence\ObjectManager as EntityManager;
use Zend\InputFilter\BaseInputFilter;

interface ValidatorClassMappingInterface
{
    public function attach(BaseInputFilter $inputFilter, EntityManager $entityManager, $className);
} 