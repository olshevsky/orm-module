<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/23/14
 * Time: 4:51 PM
 */

namespace Arilas\ORM\Mvc\Controller;


use Arilas\ORM\Form\Type\AbstractType;
use Arilas\ORM\Mvc\Controller\Plugin\GetArilas;
use Arilas\ORM\Repository\AbstractRepository;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;

/**
 * Class AbstractResource
 * @package Arilas\ORM\Mvc\Controller
 * @method GetArilas getArilas()
 * @method AbstractType createInputFilter($entity)
 */
class AbstractResource extends AbstractRestfulController
{
    /** @var  AbstractRepository */
    protected $repository;

    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException(
                'Missing route matches; unsure how to retrieve action'
            );
        }

        $request = $e->getRequest();

        // RESTful methods
        $method = strtolower($request->getMethod());
        switch ($method) {
            // Custom HTTP methods (or custom overrides for standard methods)
            case (isset($this->customHttpMethodsMap[$method])):
                $callable = $this->customHttpMethodsMap[$method];
                $action = $method;
                $return = call_user_func($callable, $e);
                break;
            // DELETE
            case 'delete':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    $action = 'delete';
                    $return = $this->delete($this->convertEntity($id, $action));
                    break;
                }

                $action = 'deleteList';
                $return = $this->deleteList();
                break;
            // GET
            case 'get':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    $action = 'get';
                    $return = $this->get($this->convertEntity($id, $action));
                    break;
                }
                $action = 'getList';
                $return = $this->getList();
                break;
            // HEAD
            case 'head':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id === false) {
                    $id = null;
                }
                $action = 'head';
                $this->head($this->convertEntity($id, $action));
                $response = $e->getResponse();
                $response->setContent('');
                $return = $response;
                break;
            // OPTIONS
            case 'options':
                $action = 'options';
                $this->options();
                $return = $e->getResponse();
                break;
            // PATCH
            case 'patch':
                $id = $this->getIdentifier($routeMatch, $request);
                $data = $this->processBodyContent($request);

                if ($id !== false) {
                    $action = 'patch';
                    $return = $this->patch($this->convertEntity($id, $action), $data);
                    break;
                }

                // TODO: This try-catch should be removed in the future, but it
                // will create a BC break for pre-2.2.0 apps that expect a 405
                // instead of going to patchList
                try {
                    $action = 'patchList';
                    $return = $this->patchList($data);
                } catch (Exception\RuntimeException $ex) {
                    $response = $e->getResponse();
                    $response->setStatusCode(405);
                    return $response;
                }
                break;
            // POST
            case 'post':
                $action = 'create';
                $return = $this->processPostData($request);
                break;
            // PUT
            case 'put':
                $id = $this->getIdentifier($routeMatch, $request);
                $data = $this->processBodyContent($request);

                if ($id !== false) {
                    $action = 'update';
                    $return = $this->update($this->convertEntity($id, $action), $data);
                    break;
                }

                $action = 'replaceList';
                $return = $this->replaceList($data);
                break;
            // All others...
            default:
                if (method_exists($this, $method)) {
                    $id = $this->getIdentifier($routeMatch, $request);
                    $action = $method;
                    if ($id !== false) {
                        $return = $this->$method($this->convertEntity($id, $action));
                        break;
                    }
                    $return = $this->$method();
                } else {
                    $response = $e->getResponse();
                    $response->setStatusCode(405);
                    return $response;
                }
        }

        $routeMatch->setParam('action', $action);
        $e->setResult($return);
        return $return;
    }

    public function convertEntity($id, $method)
    {
        $method = new \ReflectionMethod(get_class($this), $method);
        foreach ($method->getParameters() as $param) {
            $entityName = $param->getClass()->getName();
            return $this->getArilas()->getRepository($entityName)->find($id);
        }

        return null;
    }
} 