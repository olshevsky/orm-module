<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:16
 */

namespace Arilas\ORM\Mvc\Controller\Plugin;

use Arilas\ORM\EntityManager;
use Arilas\ORM\Form\Filter\FilterMappingInterface;
use Arilas\ORM\Form\Mapping\InputFilter as InputFilterAnnotation;
use Arilas\ORM\Form\Type\AbstractType;
use Arilas\ORM\Form\Validator\ValidatorClassMappingInterface;
use Arilas\ORM\Form\Validator\ValidatorMappingInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use ReflectionProperty;
use Zend\InputFilter\BaseInputFilter;
use Zend\InputFilter\Input;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\Controller\PluginManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CreateInputFilter extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    /** @var  PluginManager */
    protected $pluginManager;
    /** @var  EntityManager */
    protected $entityManager;
    /** @var  ClassMetadata */
    protected $classMetadata;
    /** @var  AnnotationReader */
    protected $reader;
    /** @var  BaseInputFilter */
    protected $filter;
    /** @var  ObjectManager */
    protected $usedEntityManager;

    /**
     * @param string|object $entity
     * @return BaseInputFilter|AbstractType
     * @throws \Arilas\ORM\Exception\RuntimeException
     */
    public function __invoke($entity)
    {
        if (is_string($entity)) {
            $entity = new $entity();
        }

        $this->prepare($entity);
        $this->createInputFilter();
        $this->parseFieldsAnnotations();
        $this->parseClassAnnotations();
        $this->prepareData($entity);

        return $this->filter;
    }

    /**
     * Fetch Metadata for Entity
     * @param object $entity
     */
    protected function prepare($entity)
    {
        $this->classMetadata = $this->entityManager->getClassMetadata(get_class($entity));
    }

    /**
     * Create InputFilter based on Annotation or create AbstractType
     */
    protected function createInputFilter()
    {
        /** @var null|InputFilterAnnotation $annotation */
        $annotation = $this->reader->getClassAnnotation(
            $this->classMetadata->reflClass,
            InputFilterAnnotation::class
        );
        if (!is_null($annotation)) {
            $this->filter = $this->getServiceLocator()->getServiceLocator()->get($annotation->serviceName);

            if ($this->filter instanceof AbstractType) {
                $this->filter
                    ->setDataClass($this->classMetadata->getName())
                    ->setHydrator($this->entityManager->getObjectHydrator());
            }
        } else {
            $this->filter = new AbstractType();
            $this->filter
                ->setDataClass($this->classMetadata->getName())
                ->setHydrator($this->entityManager->getObjectHydrator());
        }
    }

    /**
     * Get service locator
     *
     * @return PluginManager
     */
    public function getServiceLocator()
    {
        return $this->pluginManager;
    }

    /**
     * Parse Fields Annotations for adds Validators and Filters
     */
    protected function parseFieldsAnnotations()
    {
        foreach ($this->classMetadata->reflFields as $field) {
            $annotations = $this->reader->getPropertyAnnotations($field);
            $input = $this->injectInput($field);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof FilterMappingInterface) {
                    $annotation->attach($input->getFilterChain());
                } elseif ($annotation instanceof ValidatorMappingInterface) {
                    $annotation->attach($input);
                }
            }
        }
    }

    /**
     * @param ReflectionProperty $property
     * @return Input
     */
    protected function injectInput(ReflectionProperty $property)
    {
        if (!$this->filter->has($property->getName())) {
            $input = new Input($property->getName());
            $input->setRequired(false);
            $this->filter->add($input);
        }

        return $this->filter->get($property->getName());
    }

    /**
     * Parse Class Annotations for adds Validators
     */
    protected function parseClassAnnotations()
    {
        $annotations = $this->reader->getClassAnnotations($this->classMetadata->reflClass);
        foreach ($annotations as $annotation) {
            if ($annotation instanceof ValidatorClassMappingInterface) {
                $annotation->attach(
                    $this->filter,
                    $this->usedEntityManager,
                    $this->classMetadata->getName()
                );
            }
        }
    }

    /**
     * Place current data from Entity
     * @param object $entity
     */
    protected function prepareData($entity)
    {
        $this->filter->setData(
            $this->entityManager->getObjectHydrator()->extract($entity, false)
        );
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->pluginManager = $serviceLocator;
        $this->entityManager = $this->getServiceLocator()->getServiceLocator()->get('arilas.orm.entity_manager');
        $config = $this->getServiceLocator()->getServiceLocator()->get('Config');
        if (isset($config['arilas']['form']['em']) && $config['arilas']['form']['em'] != '') {
            $this->usedEntityManager = $this->getServiceLocator()->getServiceLocator()->get(
                $config['arilas']['form']['em']
            );
        } else {
            $this->usedEntityManager = $this->entityManager;
        }
        $this->reader = $this->entityManager->getMetadataFactory()->getReader();
    }
}