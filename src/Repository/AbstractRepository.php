<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/24/13
 * Time: 4:23 PM
 */

namespace Arilas\ORM\Repository;

use Arilas\ORM\Common\UnitOfWork;
use Arilas\ORM\DBAL\QueryBuilder;
use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\EntityManager;
use Arilas\ORM\Exception\RuntimeException;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;

class AbstractRepository implements ObjectRepository
{
    /** @var  ClassMetadata */
    protected $metadata;
    /** @var  EntityManager */
    protected $em;
    /** @var  UnitOfWork */
    protected $unitOfWork;

    /**
     * @param \Arilas\ORM\EntityManager $em
     * @param \Doctrine\ORM\Mapping\ClassMetadata $metadata
     */
    public function __construct(EntityManager $em, ClassMetadata $metadata)
    {
        $this->em = $em;
        $this->metadata = $metadata;
        $this->unitOfWork = new UnitOfWork($this);
    }

    /**
     * Method returns Entity by criteria
     * @param  array|null $criteria
     * @return EntityInterface|null
     */
    public function findOneBy(array $criteria)
    {
        return $this->fetchOne($this->getCriteriaQuery($criteria));
    }

    /**
     * Method returns an Entity of data by providing Query and Parameters
     * @param  string|QueryBuilder $query
     * @param  array $params
     * @return EntityInterface|null
     */
    protected function fetchOne($query, array $params = [])
    {
        if ($query instanceof QueryBuilder) {
            $params = array_replace($query->getParameters(), $params);
            $query = $query->getSQL();
        }

        $entity = $this->em->getConnection()->fetchAssoc($query, $params);
        if (!is_array($entity)) {
            return $entity;
        } else {
            return $this->unitOfWork->hydrate($entity);
        }
    }

    /**
     * @param  array|int $criteria
     * @param  array $orderBy
     * @param  int $limit
     * @param  int $offset
     * @return QueryBuilder
     */
    protected function getCriteriaQuery($criteria, array $orderBy = [], $limit = 10, $offset = 0)
    {
        $metadata = $this->getClassMetadata();
        $query = $this->createQueryBuilder($metadata->getTableName());
        foreach ($orderBy as $column => $order) {
            if (is_int($column)) {
                $query->orderBy($order);
            } else {
                $query->orderBy($column, $order);
            }
        }
        if (is_array($criteria)) {
            $query->setFirstResult($offset);
            $query->setMaxResults($limit);
            $tag = 0;
            foreach ($criteria as $column => $value) {
                if (in_array($column, $metadata->fieldNames)) {
                    $column = $metadata->getColumnName($column);
                } elseif (isset($metadata->associationMappings[$column])) {
                    $mapping = $metadata->getAssociationMapping($column);
                    $column = $mapping['joinColumns'][0]['name'];
                }
                if ($value instanceof EntityInterface) {
                    $value = $value->getId();
                } elseif (is_array($value)) {
                    $values = [];
                    foreach($value as $val) {
                        if($val instanceof EntityInterface) {
                            /** @var EntityInterface $val */
                            $values[] = $val->getId();
                        } else {
                            $values[] = $val;
                        }
                    }
                    $paramsString = implode(',', $values);
                    $query->andWhere($metadata->getTableName() . '.' . $column . ' IN (:val' . ++$tag . ')');
                    $query->setParameter(':val' . $tag, $paramsString);
                } else {
                    $query->andWhere($metadata->getTableName() . '.' . $column . ' = :tag' . ++$tag);
                    $query->setParameter(':tag' . $tag, $value);
                }
            }
        } elseif (!is_null($criteria)) {
            $identifier = $metadata->getIdentifier();
            if (count($identifier) == 1) {
                $query->where(
                    $query->expr()
                        ->eq(
                            $metadata->getTableName() . "." . $metadata->getColumnName($identifier[0]),
                            "'" . $criteria . "'"
                        )
                );
            } else {
                $query->where($criteria);
            }
        }

        return $query;
    }

    /**
     * @return ClassMetadata
     */
    public function getClassMetadata()
    {
        return $this->metadata;
    }

    /**
     * Creates a new QueryBuilder with alias
     * @param  string $alias
     * @throws \Arilas\ORM\Exception\RuntimeException
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias)
    {
        if (is_string($alias)) {
            $queryBuilder = new \Arilas\ORM\DBAL\QueryBuilder($this->getEntityManager());
            return $queryBuilder
                ->from($this->metadata->getName(), $alias)
                ->select($alias . '.*');
        } else {
            throw new RuntimeException('Alias should be a string');
        }
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Method returns an array of all Entities in Table
     * @return EntityInterface[]
     */
    public function findAll()
    {
        return $this->findBy([]);
    }

    /**
     * Method returns an array of Entities by Criteria
     * @param  array      $criteria
     * @param  array|null $orderBy
     * @param  int|null   $limit
     * @param  int|null   $offset
     * @return EntityInterface[]
     */
    public function findBy(array $criteria, array $orderBy = [], $limit = null, $offset = null)
    {
        return $this->fetchAll($this->getCriteriaQuery($criteria, $orderBy, $limit, $offset), array());
    }

    /**
     * Method returns an array of data by providing Query and Parameters, also
     * you may Cover an result array to Entity by providing userEntity flag
     * @param  string|QueryBuilder $query
     * @param  array               $params
     * @return EntityInterface[]
     */
    protected function fetchAll($query, array $params = [])
    {
        if ($query instanceof QueryBuilder) {
            $params = array_replace($query->getParameters(), $params);
            $query = $query->getSQL();
        }

        $result = $this->em->getConnection()->fetchAll($query, $params);
        if (!is_array($result)) {
            return $result;
        }
        $entities = [];
        foreach ($result as $row) {
            $entity = $this->unitOfWork->hydrate($row);
            $entities[$entity->getId()] = $entity;
        }

        return $entities;
    }

    /**
     * @param EntityInterface $entity
     * @return int
     */
    public function commit(EntityInterface $entity)
    {
        if ($this->unitOfWork->has($entity->getId())) {
            //Update entity
            return $this->update($entity);
        } elseif (!is_null($entity->getId()) && $this->find($entity->getId())) {
            return $this->update($entity);
        } else {
            //Insert entity
            return $this->insert($entity);
        }
    }

    /**
     * @param EntityInterface $entity
     * @return int
     */
    protected function update(EntityInterface $entity, $changes = null)
    {
        if ($this->metadata->hasLifecycleCallbacks(Events::preUpdate)) {
            $this->invokeLifecycleCallbacks(Events::preUpdate, $entity);
        }
        if ($this->metadata->hasLifecycleCallbacks(Events::preFlush)) {
            $this->invokeLifecycleCallbacks(Events::preFlush, $entity);
        }
        $this->getEntityManager()->getEventManager()->trigger(
            EntityManager::PRE_UPDATE_EVENT,
            $this,
            [
                'entity' => $entity,
                'origin' => $this->unitOfWork->getOrigin($entity->getId()),
            ]
        );
        if (is_null($changes)) {
            $changes = $this->unitOfWork->calculateChanges($entity);
        }
        $result = false;
        if (!empty($changes)) {
            $identifier = [];
            foreach ($this->metadata->getIdentifier() as $id) {
                $columnName = $this->metadata->getColumnName($id);
                $identifier[$columnName] = $entity->getId();
                break;
            }
            $result = $this->em->getConnection()->update(
                $this->getClassMetadata()->getTableName(),
                $changes,
                $identifier
            );
            $this->unitOfWork->updateOrigin($entity);
        }
        $this->getEntityManager()->getEventManager()->trigger(
            EntityManager::POST_UPDATE_EVENT,
            $this,
            ['entity' => $entity]
        );
        return $result;
    }

    protected function invokeLifecycleCallbacks($lifecycleEvent, EntityInterface $entity)
    {
        foreach ($this->metadata->getLifecycleCallbacks($lifecycleEvent) as $callback) {
            $entity->$callback();
        }
    }

    /**
     * Method uses to find some Entity bu its Identifier
     * @param  int $id
     * @return EntityInterface
     */
    public function find($id)
    {
        if (is_null($id)) {
            return null;
        }
        if ($this->unitOfWork->has($id)) {
            return $this->unitOfWork->get($id);
        }

        return $this->fetchOne($this->getCriteriaQuery($id));
    }

    /**
     * @param EntityInterface $entity
     * @return int
     */
    protected function insert(EntityInterface $entity)
    {
        if ($this->metadata->hasLifecycleCallbacks(Events::prePersist)) {
            $this->invokeLifecycleCallbacks(Events::prePersist, $entity);
        }
        if ($this->metadata->hasLifecycleCallbacks(Events::preFlush)) {
            $this->invokeLifecycleCallbacks(Events::preFlush, $entity);
        }
        $this->getEntityManager()->getEventManager()->trigger(
            EntityManager::PRE_INSERT_EVENT,
            $this,
            [
                'entity' => $entity,
                'origin' => null,
            ]
        );
        $changes = $this->unitOfWork->calculateChanges($entity);
        $result = $this->em->getConnection()->insert(
            $this->getClassMetadata()->getTableName(),
            $changes
        );
        $id = $this->em->getConnection()->lastInsertId();
        $entity->setId($id);
        $this->unitOfWork->push($entity);
        $this->getEntityManager()->getEventManager()->trigger(
            EntityManager::POST_INSERT_EVENT,
            $this,
            ['entity' => $entity]
        );
        return $result;
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database
     * @return int
     */
    public function flush()
    {
        $changes = $this->getUnitOfWork()->computeChangeSets();
        $count = 0;
        foreach ($changes as list($entity, $change)) {
            $this->update($entity, $change);
            $count++;
        }

        return $count;
    }

    /**
     * @return UnitOfWork
     */
    public function getUnitOfWork()
    {
        return $this->unitOfWork;
    }

    /**
     * @param EntityInterface $entity
     * @return int
     */
    public function remove(EntityInterface $entity)
    {
        $this->getEntityManager()->getEventManager()->trigger(
            EntityManager::PRE_DELETE_EVENT,
            $this,
            ['entity' => $entity]
        );
        $result = false;
        if ($this->unitOfWork->has($entity->getId())) {
            $identifier = [];
            foreach ($this->metadata->getIdentifier() as $id) {
                $columnName = $this->metadata->getColumnName($id);
                $identifier[$columnName] = $entity->getId();
                break;
            }
            $result = $this->em->getConnection()->delete(
                $this->getClassMetadata()->getTableName(),
                $identifier
            );
            $this->unitOfWork->remove($entity->getId());
        }
        $this->getEntityManager()->getEventManager()->trigger(
            EntityManager::POST_DELETE_EVENT,
            $this,
            ['entity' => $entity]
        );

        return $result;
    }

    /**
     * Method returns name of Entity
     * @return string
     */
    public function getClassName()
    {
        return $this->getClassMetadata()->getName();
    }
}
