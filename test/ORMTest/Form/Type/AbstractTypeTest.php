<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 11:31
 */

namespace Arilas\ORMTest\Form\Type;


use Arilas\ORM\Form\Type\AbstractType;
use Arilas\ORM\Mvc\Controller\Plugin\CreateInputFilter;
use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Bootstrap;
use Arilas\ORMTest\Test\Test;
use Zend\Mvc\Controller\PluginManager;

class AbstractTypeTest extends \PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testHydrate()
    {
        $sm = Bootstrap::getServiceManager();
        /** @var PluginManager $pluginManager */
        $pluginManager = $sm->get('Zend\Mvc\Controller\PluginManager');
        /** @var CreateInputFilter $createInputFilter */
        $createInputFilter = $pluginManager->get('createInputFilter');

        /** @var AbstractType $type */
        $type = $createInputFilter(Test::class);

        $this->assertSame(Test::class, $type->getDataClass());

        $data = [
            'value' => ' test ',
        ];

        $type->setData($data);
        /** @var Test $rawObject */
        $rawObject = $type->getRawObject();
        /** @var Test $object */
        $object = $type->getObject();

        $this->assertInstanceOf(Test::class, $rawObject);
        $this->assertInstanceOf(Test::class, $object);
        $this->assertSame($data['value'], $rawObject->getValue());
        $this->assertSame(trim($data['value']), $object->getValue());
        $this->assertFalse($type->isValid());
        $this->assertCount(1, $type->getInvalidInput());
        $this->assertSame(
            'Value must to be a Email Address',
            array_values($type->get('value')->getMessages())[0]
        );
    }
}
 