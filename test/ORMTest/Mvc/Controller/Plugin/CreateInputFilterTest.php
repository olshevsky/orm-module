<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 5:09
 */

namespace Arilas\ORMTest\Mvc\Controller\Plugin;

use Arilas\ORM\Mvc\Controller\Plugin\CreateInputFilter;
use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Bootstrap;
use Arilas\ORMTest\Test\Test;
use Zend\Filter\FilterChain;
use Zend\InputFilter\BaseInputFilter;
use Zend\InputFilter\Input;
use Zend\Mvc\Controller\PluginManager;

class CreateInputFilterTest extends \PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testInstance()
    {
        $sm = Bootstrap::getServiceManager();
        /** @var PluginManager $pluginManager */
        $pluginManager = $sm->get('Zend\Mvc\Controller\PluginManager');
        /** @var CreateInputFilter $createInputFilter */
        $createInputFilter = $pluginManager->get('createInputFilter');

        $this->assertInstanceOf(CreateInputFilter::class, $createInputFilter);
        /** @var BaseInputFilter $filter */
        $filter = $createInputFilter(Test::class);

        $this->assertInstanceOf(BaseInputFilter::class, $filter);
        $this->assertInstanceOf(Input::class, $filter->get('id'));
        $this->assertFalse($filter->get('id')->isRequired());
        $this->assertInstanceOf(Input::class, $filter->get('value'));
        $this->assertTrue($filter->get('value')->isRequired());
        /** @var FilterChain $filterChain */
        $filterChain = $filter->get('value')->getFilterChain();
        $this->assertSame(3, $filterChain->count());
    }
}
 