<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/26/13
 * Time: 6:57 PM
 */
namespace Arilas\ORMTest\Test;

use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\Form\Filter\Mapping as Filter;
use Arilas\ORM\Form\Validator\Mapping as Validator;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Test
 * @ORM\Entity
 * @ORM\Table(name="tests")
 * @Validator\UniqueFields("value")
 * @ORM\HasLifecycleCallbacks()
 */
class Test implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Filter\StringTrim
     * @Filter\StripTags
     * @Filter\HtmlEntities
     * @Validator\Required
     * @Validator\StringLength(min=3)
     * @Validator\EmailAddress(message="Value must to be a Email Address")
     */
    protected $value;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @var array
     */
    protected $array = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $datetime;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getArray()
    {
        return $this->array;
    }

    /**
     * @param array $array
     */
    public function setArray($array)
    {
        $this->array = $array;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * @ORM\PrePersist()
     */
    public function preInsert()
    {
        $this->datetime = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->datetime = new \DateTime();
    }
}